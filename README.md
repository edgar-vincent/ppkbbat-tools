# ppkbbat-tools

A couple of tools to read and control the battery of the PinePhone Keyboard Case, including automatic power management.

## Content

1. [Important Notices](#important-notices)
2. [Introduction](#introduction)
3. [Usage](#usage)
	1. [ppkbbat-info](#ppkbbat-info)
	2. [ppkbbat-d](#ppkbbat-d)
		1. [General Usage](#general-usage)
		2. [Charge Mode Explanation](#charge-mode-explanation)
		3. [Parallel Charging](#parallel-charging)
		4. [Sequential Charging](#sequential-charging)
		5. [Examples](#examples-1)
		6. [Running as a Background Service](#running-as-a-background-service)
		7. [Calling ppkbbat-d Once Periodically](#calling-ppkbbat-d-once-periodically)
	3. [ppkbbat-setlimit](#ppkbbat-setlimit)
		1. [General Usage](#general-usage-1)
		2. [Permanent Limit](#permanent-limit)
	4. [ppkbbat-notif](#ppkbbat-notif)
	5. [ppkbbat-toggle](#ppkbbat-toggle)
	6. [Config File](#config-file)
4. [Issues](#issues)
	1. [OG PP current is always positive](#og-pp-current-is-always-positive)
	2. [Charging stops when plugged in](#charging-stops-when-plugged-in)
	3. [Automatic suspension inhibited / setting `online` breaks reading/writing values](#automatic-suspension-inhibited-setting-online-breaks-reading-writing-values)
	4. [Wrong keyboard current while charging](#wrong-keyboard-current-while-charging)
5. [Further Reading](#further-reading)

## Important Notices

**Previously it was advised not to use use the phone’s USB port at all when the Keyboard Case is attached.[^1] [^2] [^3] [^4] However, Megi has done some tests and concluded that using either port by itself should be safe, only using both at once has to be avoided.[^5] This means that plugging a power supply into the phone to only charge its battery as well as using a (powered) dock is possible after all.**

Use a 5V 3A **PD** power supply if possible.[^6] If you plug a power supply into the keyboard and your phone reports that it alternates between being charged and not being charged, the power supply is supplying too much power and the keyboard is shutting off as a safety measure. Using power supplies supplying too little power (like ≤ 5V 1.5A) could have the supply run too hot and/or break.

Megi is working on proper PPKB power management that will be integrated into the kernel. Keep an eye out for announcements on [Megi’s blog](https://xnux.eu/log/). Once it’s done, all of this won’t be necessary anymore, at least if you use a distro with an up-to-date kernel. Those distros with older kernels will still require this until they update.

[kbpwrd](https://github.com/estokes/pinephone-kbpwrd) is another tool for automatic power management like ppkbbat-d and most likely more efficient, you should give it a try as well.

## Introduction

The default behaviour of the PinePhone Keyboard battery is not optimal and presents the user with some issues. These are mostly caused by two things: The charging priority which is `KB battery` -> `PP` -> `PP battery` and the input current limits which are only `0.5A` for the PP and `2.3A` for the KB. This results in the phone receiving very little power from the KB battery or power supplies, and even when this is modified, the aforementioned order of priorities still makes especially charging with power supplies very difficult. The keyboard battery will take up 2.3A first and only pass the remainder through to the phone. Only when the keyboard battery is full will the priority switch to the phone and supply it with the full amount the power supply can provide. So, especially with the unchanged default behaviour, a power supply as recommended, 5V 3A PD, is a minimum. Supplying 3A will leave about 0.6A for the phone while the Keyboard battery is charging and has priority, which it will have for a couple of hours due to its high capacity. Power supplies with less power may be used but definitely require power management as provided by [ppkbbat-d](#ppkbbat-d).

Now imagine the situation of both batteries being low. You plug a power supply into the keyboard’s USB port. The keyboard battery takes priority and receives 2.3A, leaving the phone with 0.6A. 0.6A is about what a PinePhone uses if not doing anything too power intensive. This means, in the best scenario, the PinePhone’s battery will stay constant for a few hours until the keyboard battery is full, and only then start charging. But if the phone uses more power, the percentage will continue to go down for those few hours, possibly reaching 0%. And if you have a power supply with only 1A or 2A, the phone won’t get anything, and on top of that it will take a lot longer for the keyboard battery to become full, so the phone will most likely go down to 0% first.

It’s currently not possible to influence the charging priority. It is possible to change the input current limits for the keyboard battery and phone battery. Adjusting them dynamically according to the current situation achieves a similar effect, for example setting the keyboard battery’s input limit to the minimum passes more power from a connected power supply through to the phone.

Now, ppkbbat-tools gives you two main options for this: You can either use [ppkbbat-d](#ppkbbat-d) to have the limits adjusted automatically as needed, or you can use [ppkbbat-setlimit](#ppkbbat-setlimit) to permanently set the PP and KB limit to something that works well enough for you in all cases. The minimum you should do is set the PP limit to something higher than the default 0.5A since that is barely enough to keep up with what the phone uses and not enough to properly charge it.

**I recommend using ppkbbat-d as described in [Calling ppkbbat-d Once Periodically](#calling-ppkbbat-d-once-periodically).**

## Usage

## ppkbbat-info

This tool fetches all battery values and outputs them into a nice table (or one line), allowing you to monitor the batteries and debug charging/discharging behaviour.

```
ppkbbat-info [options] <interval>

  <interval>  The update interval in seconds. Default is 1. Use 0 to run once.

Options:

  -r  Refresh the output instead of printing new instances.
  -l  Log mode that outputs single lines instead of a fancy table.
  -m  Add Megi's ppkb-i2c-charger-ctl info values.
```

Megi’s values of course also require [ppkb-i2c-charger-ctl](https://xff.cz/git/pinephone-keyboard/) (also available on the AUR as `pinephone-keyboard-git`) being present in your $PATH, as well as ppkbbat-info being run as root for i2c access. These values can be useful especially when the sysfs values are wrong as it currently sometimes appears to be the case with the [KB current when charging](#wrong-keyboard-current-while-charging), or if your distro doesn’t have a PPKB driver at all, as is/was the case with Manjaro.

To print the results to the terminal and also to a log file, append ` | tee path/to/file.log` to the command, e.g. `ppkbbat-info -l | tee ~/ppkbbat.log`. To only write to a log file and not output to the terminal, append  ` > path/to/file.log`. Of course you don’t have to use `-l` to log it to a file, you can log the default table mode as well. You might not want to use `-r` when writing to a log file since that will also only write one instance to the file and then update that, so you won’t really have a log. All options can be combined in any way.

### Examples

```console
[phalio@tatl:~]$ ppkbbat-info
| 21:56:34 | PP Battery  | KB Battery  |
|----------+-------------+-------------|
| Current  |  -0.101000A |  -0.649753A |
| C. Limit |   1.200000A |   0.500000A |
| KB>PP L. |   0.500000A |             |
| Voltage  |   4.131000V |   3.966110V |
| OCV      |   4.148100V |   4.016330V |
| Capacity |         98% |         94% |
| Status   |    Charging | Discharging |
| Online   |           1 |           1 |
```

```console
[phalio@tatl:~]$ ppkbbat-info -l
21:58:15 ppcurrent='-0.121'    kbcurrent='-0.648261' pplimit='1.2' kblimit='0.5' kb2pplimit='0.5' ppvoltage='4.127'    kbvoltage='3.9637'   ppocv='4.147'    kbocv='4.01418'  ppcapacity='98' kbcapacity='94' ppstatus='Charging'     kbstatus='Discharging'  pponline='1' kbonline='1' 
21:58:17 ppcurrent='-0.195'    kbcurrent='-0.651245' pplimit='1.2' kblimit='0.5' kb2pplimit='0.5' ppvoltage='4.115'    kbvoltage='3.96343'  ppocv='4.147'    kbocv='4.01418'  ppcapacity='98' kbcapacity='94' ppstatus='Charging'     kbstatus='Discharging'  pponline='1' kbonline='1'
```

As you may notice, there are three current limit values. The PP limit, the KB limit and the PP -> KB limit. Usually when I or others refer to the PP limit, we are referring to the PP -> KB limit that defines what the phone takes via the pins on the back. The PP limit itself seems to have little influence on anything when using the keyboard, however it does seem to also apply. Mostly charging seemed to always be limited to 1.2A, but under heavy loads it seemed to go above that. Maybe that limit only applies to the phone’s battery and during high usage the phone itself can also take more from the keyboard, making the total go above 1.2A. I have no idea. I included it in the table just to be save but you should mostly just ignore it.

## ppkbbat-d

This tool recognises the current power state and automatically sets the input limits to achieve the best behaviour. When connected to a power supply, it charges both batteries at once relative to their maximum capacity so that both become full at roughly the same time (alternatively it charges the phone battery first and the keyboard battery second). When discharging, it discharges both batteriesbut will make sure the phone battery doesn’t fully discharge if the keyboard battery still holds power. When the phone has an increased power need due to power intensive things running, it will make sure to provide the phone with more power. `Keyboard battery` -> `phone` charging is never entirely deactivated, it always provides the phone with a little power. This means that only the keyboard battery will slowly discharge when the phone uses very little power or it will charge the phone battery very slowly, especially during suspension. It also means that ecosystems/distros that inhibit automatic suspension when the phone battery is being charged won’t let the phone automatically suspend. If you experience this issue, see [this](#automatic-suspension-inhibited-setting-online-breaks). Balancing charging as well as discharging between both batteries at the same time is better for battery life than charging/discharging only one at a time at full power, while still retaining the maximum charge speed of a power supply.

### General Usage

```
ppkbbat-d [options] <interval>

  <interval>  The update interval in seconds. Default is 1. Use 0 to run once.

Options:

  -q  Sequential charge mode (charge PP first, then KB), default is parallel charge mode.
  -s  Silent mode that prints no log messages.
```

You can just use it in a terminal to see if it works, what it does and to just play around with it. Like I did during *many* charges and forced discharges with artificial high usage to test it.

ppkbbat-d features two charge modes: Parallel (default) and sequential (-q). Parallel charging is most likely the better choice. If you don’t use a 5V 3A charger, you will have to make an adjustment in the config file as described below under [Parallel Charging](#parallel-charging) or [Sequential Charging](#sequential-charging). No matter what mode or custom config you use, it will always make sure the PP is not losing charge when a power supply is connected. Unfortunately, with the [broken original PinePhone current value in older kernel versions](#og-pp-current-is-always-positive), this might not always work. If your current value is bugged and you regularly have such high PP power usage during charging that the percentage decreases with ppkbbat-d active, decrease the KB charge limit in the config file.

### Charge Mode Explanation

The default and recommended mode charges both batteries in parallel relative to their maximum capacities, so the KB receives about twice as much as the PP to have both reach 100% at roughly the same time. This allows suspending the device while charging (which Sxmo does by default) as there is no need to change priority from PP to KB, which ppkbbat-d can’t do during suspend. It also improves battery life as slower charging is healthier and it is faster than sequential mode (about 4h instead of about 5h) and as fast as kernel-managed charging. I believe this has two reasons:

One factor is the batteries accepting less and less power as their percentages increase which ppkbbat-d does not account for. This results in less power from the power supply actually being used as the percentage increases, power that could and should be used by the other battery. But ppkbbat-d -q directs almost all power to one battery at a time until it’s nearly full instead of dynamically allocating the remainder to the other battery, as non-ppkbbat-d behaviour does.

The other factor, as the following theoretical calculations suggested before I tried parallel charging in practice, is related to the fact that charging one battery after the other allows the PP to charge at 1.2A and the KB to charge at up to 2.3A, while charging them together would charge the PP at about 0.8A (more or less of course depending on what the phone uses if not suspended) and the KB at 1.6A. If we assume that, for simplicity, the PP battery takes one hour to charge at 3A and the twice as big KB battery two hours at 3A, the hypothetical charge time when charging them **one after the other** with the aforementioned real current values would be:

```
PP_charge_time = (3A ÷ 1.2A) × 1h = 2.5h
KB_charge_time = (3A ÷ 2.3A) × 2h = 2.6h
total_charge_time = $PP_charge_time + $KB_charge_time = 5.1h
```

Charging them **at the same time** would mean the following, taking the longer of the two time spans as the total time since both occur at the same time:

```
PP_charge_time = (3A ÷ 0.8A) × 1h = 3.75h
KB_charge_time = (3A ÷ 1.6A) × 2h = 3.75h
total_charge_time = MAX ($PP_charge_time : $KB_charge_time) = 3.75h
```

Parallel charging in the above theory supplies both batteries with a total of 2.4A while sequential charging supplies an average of `(1.2A + 2.3A + 2.3A) ÷ 3 = 1.93A` (the actual value would be a bit higher since the non-prioritised battery also receives a little). Together they can make use of the full amount of power received by a power supply, but alone they can only take up to 1.2A/2.3A. Due to the behaviour as described and shown [here](#general-ppkbbat-setlimit-usage), decreasing the KB limit appears to be lossy, as decreasing the KB limit by x increases the amount of power the phone gets by only x/2. Sequential charging can be improved a bit if using a 3A power supply since that is enough to provide a bit more power than what the PP can take, so the KB limit can be set a bit higher. I set this as the default (0.8A KB limit) since 3A power supplies should be used and this makes both modes work for 3A by default, but it means that people using power supplies with less than 3A will have to use the config file for sequential charging as well to decrease the KB limit to its minimum.

### Parallel Charging

Parallel charging is specifically tuned for charging with 5V 3A supplies by default, so you might experience issues with lower-power supplies. It’s supposed to charge the KB at twice the amount of current as the PP, since its maximum capacity is about twice as high. Use ppkbbat-info to check the current values while charging, and keep in mind that the PP will display lower net input currents if it’s doing something more power-intensive. If the currents are too far off of a 1:2 ratio during average charge conditions, you can customise the amount of power the KB and PP get to fit your needs. Edit the file `sudo nano /etc/ppkbbat-tools` and add the following line:

```
parallel_kb_charge_limit=1500000
```

Adjust this number, restart ppkbbat-d and see how the current values change using ppkbbat-info. Lowering the number will decrease what the KB receives and therefore increase what the PP receives.

### Sequential Charging

This mode charges the PP to full first and only then the KB. This is worse for battery life, it won’t switch to KB charging when the PP becomes full during suspension (e.g. in Sxmo that suspends while charging by default) and it takes longer (see the table below). But it always makes sure the PP doesn't discharge when a power supply is plugged in and it could also be useful if you detach the keyboard a lot and you therefore want to make sure the PP itself is charged as quickly as possible first. Sequential charging is specifically tuned for charging with 5V 3A supplies by default, so you will experience issues with lower-power supplies. If you use a power supply with less than 3A, edit the file `sudo nano /etc/ppkbbat-tools` and add the following line:

```
sequential_kb_charge_limit=500000
```

This will make sure that (almost) all power from the power supply goes to the PP first when it has to charge. The default value as defined in ppkbbat-d is 800000. Here are some test results of different `sequential_kb_charge_limit` values with a 5V 3A power supply, Arch Sxmo and the screen off (time to full is measured from pp 10% kb empty to both have reached status full):

| sequential_kb_charge_limit | PP current during KB priority | KB current during priority | Time to both full          |
|:---------------------------|:------------------------------|:---------------------------|:---------------------------|
| 2000000                    | 0.6A                          | 2.1A                       | 4h (same as parallel mode) |
| 500000                     | 1.2A                          | 0.14A                      | 5.5h                       |
| 700000                     | 1.2A                          | 0.44A                      | 5h                         |
| 800000                     | 1.2A                          | 0.78A                      | 5h                         |
| 900000                     | 1.15A                         | 0.89A                      | 4.75h                      |

I chose 800000 as the default value as it’s the first current limit value where the amount the KB actually gets corresponds to the limit, so it gets a fair amount while the PP should still also get its maximum 1.2A unless it’s doing something more power-intensive, although there seemed to have been not that much of a difference to the KB getting only 0.14A. But in any case, ppkbbat-d sequential mode is slower than parallel mode and parallel mode is as fast as charging managed by the kernel.

### Examples

```console
[amarizo@squirtle:~]$ sudo ppkbbat-d
21:40:31 Discharging, low usage: Low KB->PP powering
[…]
22:18:13 Discharging, high usage: Medium KB->PP powering
[…]
22:36:26 Discharging, PP low: Keep PP above 25%
[…]
16:36:47 KB off/empty: Ready for power supply
[…]
16:10:42 Charging: Charge both
[…]
16:18:61 Charging, high usage: Max PP powering
[…]
20:10:42 Charging, KB full: Charge PP
[…]
```

```console
[lily@nefēly:~]$ sudo ppkbbat-d -q
16:10:42 Charging: Charge PP first
[…]
16:48:48 Charging, PP full, low usage: Charge KB
[…]
16:51:53 Charging, PP full, high usage: Charge KB a bit
[…]
20:10:42 Charging, KB full: Charge PP
[…]
```

### Running as a Background Service

You can have ppkbbat-d running automatically in the background to always manage the limits for you, for example using a systemd service.

Use `sudo nano /etc/systemd/system/ppkbbat-d.service` and add the following lines (adjust the path to ppkbbat-d to fit yours):

```
[Unit]
Description=PinePhone Keyboard battery power management

[Service]
Type=simple
ExecStart=/home/alarm/git/ppkbbat-tools/ppkbbat-d
Restart=on-failure
RestartSec=10

[Install]
WantedBy=multi-user.target
```

Activate the service by using `sudo systemctl enable --now ppkbbat-d.service`. To minimise the amount of resources it uses, you could add a number at the end of the ExecStart line to lengthen the update interval in seconds as described [here](#ppkbbat-d). If you don’t need the log messages and you’re bothered by it spamming your journal, use -s. To access the log messages when it runs via systemd, use `journalctl -f | grep "tatl ppkbbat-d"` (replace "tatl" with the name of your device (the thing behind @ like [phalio@tatl:~]$)). Omit the -f for journalctl to see the entire log history instead of a live view.

### Calling ppkbbat-d Once Periodically

Probably the least resource-intensive way is having systemd, cron or the like run it once periodically with a bigger interval. This should also circumvent [this bug](#charging-stops-when-plugged-in). I currently use this method and it has been working perfectly.

#### systemd:

Use `sudo nano /etc/systemd/system/ppkbbat-d-0.service` and add the following lines (adjust the path to ppkbbat-d to fit yours):

```
[Unit]
Description=PinePhone Keyboard battery power management
Wants=ppkbbat-d-0.timer

[Service]
Type=simple
ExecStart=/home/alarm/git/ppkbbat-tools/ppkbbat-d 0

[Install]
WantedBy=default.target
```

Then create the timer that will periodically activate this service using `sudo nano /etc/systemd/system/ppkbbat-d-0.timer` and add the following lines:

```
[Unit]
Description=PinePhone Keyboard battery power management

[Timer]
OnCalendar=minutely
Persistent=true

[Install]
WantedBy=timers.target
```

Lastly activate the timer by using `sudo systemctl enable --now ppkbbat-d-0.timer`.

#### cron:

Use `sudo crontab -e` to open the crontab for the root user and add the following line:
```
* * * * * /home/alarm/git/ppkbbat-tools/ppkbbat-d 0
```
This will run ppkbbat-d once every minute. Adjust the path to ppkbbat-d to fit yours.

## ppkbbat-setlimit

This allows you to conveniently set both input current limits manually. You can also use it to define a permanent set of limits instead of using dynamic management.

### General Usage

```
ppkbbat-setlimit <pp-limit> <kb-limit>

Limits are in µA. Not specifying a limit will set the PP to 1500000 and the KB to 1500000 unless other defaults are specified in the config file. The default unchanged value for the PP is 500000 and for the KB 2300000.
```

Both limits have limited predefined sets of values that they accept, so you can’t specify any arbitrary value, but that also means that you don’t have to worry about setting it too high. It will always go to the nearest allowed value. If you frequently use a specific set of values, you can change ppkbbat-setlimit’s default values by editing the file `sudo nano /etc/ppkbbat-tools` and adding the following lines:

```
setlimit_pp=1500000
setlimit_kb=1500000
```

Setting both to 1500000 works well for 5V 3A power supplies, as explained in [Parallel Charging](#parallel-charging). For power supplies supplying less power you should choose a lower KB limit.

To increase the amount of power the phone gets from power supplies while charging, lower the keyboard limit and increase the phone limit. You can take a look at the code of ppkbbat-d to get an understanding of what value pairs are useful in what situations.

Unfortunately, decreasing the keyboard limit by x ampere while charging does not increase the amount the phone gets by x ampere. Instead, it seems to be pretty much exactly x/2 as the following table from my experiments shows. Decreasing the keyboard’s current limit by 0.8A increased the phone’s current by only 0.4A and so on.

| KB limit | PP current   | KB current   |
|:---------|:-------------|:-------------|
| KB off   | -0.35A       | 0A           |
| 2.3A     | 0.1A         | 2.4A         |
| 1.5A     | 0.5A  (+0.4) | 1.6A  (-0.8) |
| 0.5A     | 1.2A  (+0.7) | 0.2A  (-1.4) |

*The values of the table above were taken with my USB cable that does not pass through the full 3A but instead about 2.4A. The PP current values while charging would be higher with full 3A being supplied: 0.4A, 0.8A, 1.2A; 1.2A is the limit as described in the last paragraph of [ppkbbat-info examples](#examples)*

The minimum for both limits is 0.5A, but while setting the PP limit to 0.5 will make it receive 0.5A, setting the KB limit to 0.5A will make it receive a lot less, like 0.2A in the table above. Setting the KB limit to 0.8A is the first that will actually make the KB receive that value, as can be seen in the [parallel charging table](#parallel-charging).

### Permanent Limit

Setting the limits is not permanent. It resets not only after each boot but also each time a power supply is removed. The best way to set a permanent limit is to have something automatically set it when a power supply presence state has changed. Systemd unfortunately can’t control services based on the presence of a power supply according to the [Arch Wiki](https://wiki.archlinux.org/title/Power_management#Using_a_script_and_an_udev_rule) but udev rules work. A less elegant way would be to just have cron, systemd or the like set it periodically, just like described [here](#calling-ppkbbat-d-once-periodically) for ppkbbat-d. If you don’t use automatic power management like ppkbbat-d or kbpwrd, you at least have to set the phone’s limit to something higher than the default 0.5A as that is not enough to charge it. Having at least this or some other permanent limits set up also helps in case something goes wrong with automatic limit managers. You could also just set a pair of limits that provides a minimum amount of functionality in all states, like `1500000` for the PP to have it charge properly and something lower for the KB like `1500000` that would make sure the PP receives at least some power when connected to a power supply.

This is how you set limits permanently using a udev rule. I used this for the first months of having the PPKB.

Use `sudo nano /etc/udev/rules.d/powersave.rules` and add the following lines (adjust the path to ppkbbat-setlimit to fit yours):

```
SUBSYSTEM=="power_supply", ATTR{online}=="0", RUN+="/home/phalio/git/ppkbbat-tools/ppkbbat-setlimit true"
SUBSYSTEM=="power_supply", ATTR{online}=="1", RUN+="/home/phalio/git/ppkbbat-tools/ppkbbat-setlimit false"
```

## ppkbbat-notif

This simply pops up a GUI notification telling you the percentages of both batteries as well as the keyboard battery’s OCV. This is most useful for GUIs/distros that don’t show the keyboard’s percentage in the top info bar. For convenience, you could call it via a keyboard shortcut. I don’t know about Phosh, but keyboard shortcuts can be set up in Plasma Mobile and Sxmo. For Plasma Mobile, install `systemsettings` and `khotkeys`, then open the newly installed normal desktop system settings and edit shortcuts as you would on regular Plasma. The OCV is also very useful, especially for distros that use an older kernel version that doesn’t support reporting the keyboard battery’s capacity in percentage, and at least right now also in general since the percentage appears to be not that reliable and extremely fluctuating. Around 4.21V is 100%, 3V is 0%.[^7]

```console
[kaddi@hal:~]$ ppkbbat-notif
```

This creates a GUI notification that looks like this:

```mermaid
graph TD;
A(<b>Batteries</b><br>PP: 98%<br>KB: 94%, 4.016330V);
```

## ppkbbat-toggle

This simply toggles `keyboard battery` -> `phone` charging on and off (not `power supply` -> `phone`), just like the physical button on the side of the keyboard, except that the keyboard still reports its values to the phone, so you don’t lose the ability to see its percentage. Just like the button, this can be used to use the extra battery more like a power bank, as in charging the internal battery once it’s low. This is not the most optimal thing to do as it is a lossy process and requires manual control, however it it could be useful to circumvent issues with some distros/GUIs, for example if they refuse to suspend if the phone battery is in a charging state, which it always is with the keyboard battery supplying power. I used this for quite a while before I made ppkbbat-d, of course bound to a [keyboard shortcut](#ppkbbat-notif).

```console
[alarm@tanz:~]$ sudo ppkbbat-toggle
PPKB battery output deactivated
[alarm@tanz:~]$ sudo ppkbbat-toggle
PPKB battery output activated
```

It uses Megi’s tools and therefore requires [ppkb-i2c-charger-ctl](https://xff.cz/git/pinephone-keyboard/) (also available on the AUR as `pinephone-keyboard-git`) being present in your $PATH, as well as ppkbbat-toggle being run as root for i2c access. This is of course less optimal for keyboard shortcuts. To solve this, you can define ppkb-i2c-charger-ctl and ppkbbat-toggle to not require a password. Use `sudo visudo` (if you prefer a specific text editor, use e.g. `sudo EDITOR=nano visudo` and add the following line (replace the username and paths with yours):

```
phalio ALL=NOPASSWD: /home/phalio/git/pinephone-keyboard/ppkb-i2c-charger-ctl, /home/phalio/git/ppkbbat-tools/ppkbbat-toggle
```

Now they still require using `sudo` but you won’t have to provide a password anymore.

## Config File

You may create a config file at /etc/ppkbbat-tools to override certain predefined values. This is a list of all currently existing configurable values. Read their related chapters in this readme for details about when and how you should use them.

```
parallel_kb_charge_limit=
sequential_kb_charge_limit=
setlimit_pp=
setlimit_kb=
```

## Issues

ppkbbat-d reporting a wrong state for one or a couple of seconds is normal and caused by seemingly random fluctuations of the values it observes to determine the current state and desired behaviour. Only consider longer periods of wrong states a bug. If you report a bug, it would be helpful if you can describe exactly what the circumstances were and possibly even provide a log of both ppkbbat-d and ppkbbat-info. You can log their outputs by using e.g. `sudo ppkbbat-d | tee ~/ppkbbat-d.log` and `ppkbbat-info -l | tee ~/ppkbbat-info.log`. Some issues might be caused by ppkbbat-d, others seem to be outside of my/general control. Pray to the lord above (also known as Megi) that kernel PPKB power management will be done soon.

Below follows a condensed list of some bugs and undesired behaviours. If you encounter bugs not listed here or need any help, open an [issue](https://codeberg.org/phalio/ppkbbat-tools/issues) or contact me via any of [these methods](https://phal.io/links). I’m always happy to help :)

### OG PP current is always positive

This is a known bug that has already been fixed in newer kernel versions (I’ve never been so happy upon seeing a minus). ppkbbat-d has mechanisms that should make it work perfectly fine with the bugged current value with one exception: Recognising very high PP power demand during charging. Read the last paragraph of [ppkbbat-d general usage](#general-usage) for a workaround if your PP percentage goes down during high usage while charging.

### Charging stops when plugged in

The symptoms of this bug are: A power supply is plugged into the KB and it either stops reporting any values (ppkbbat-d prints error#1) or the KB status is `Not charging` (ppkbbat-d prints error#2).

When I was using the old PPKB kernel driver + Manjaro + Plasma Mobile, while charging, both ppkbbat-d and [kbpwrd](https://github.com/estokes/pinephone-kbpwrd) caused the KB to stop accepting power, reporting status `Not charging`. This seemingly bugged state will persist until the power supply is replugged and it happened at least once per charge, making either tool basically useless. There are also other similar bugs. The KB may stop reporting any values and either require re-plugging the charger or it might require using the button to shut it off and on again. Even using ppkbbat-d with a very slow update interval caused this bug happening just as often. The developer of kbpwrd apparently didn’t experience any of this. Maybe it’s a per-device issue, maybe they use the PPP. I have no idea if it’s possible to fix this, but it must somehow be related to changing the values as it doesn’t happen with no power management. Now using the new PPKB kernel driver + Arch + Sxmo, I believe I’m experiencing the same/similar issues. Sometimes it works perfectly fine for an entire charge or discharge, sometimes it has one of these breaking bugs constantly. But it appears to happen less often. I also noticed that the PP itself apparently continues charging perfectly fine. If you experience any of this too often, you have these options:

1. **Inconvenient, good functionality:** Run ppkbbat-d manually whenever you need the limits to change, so at least to charge the PP, then to charge the KB, then to discharge. I did that for a while with my lower-amp charging cable that required me to use limit management and it worked perfectly, albeit not the most conveniently. A [keyboard shortcut](#ppkbbat-notif) might also make it more convenient.

2. **Convenient, good functionality:** Instead of having ppkbbat-d running with a slow update interval, try having e.g. systemd or cron run it/kbpwrd once periodically as described [here](#calling-ppkbbat-d-once-periodically).

3. **Convenient, minimal functionality:** Don’t use ppkbbat-d/kbpwrd and instead use a charger and USB cable that supply full 5V 3A so that the PP always receives at least 0.6A when charging and permanently increase the PP limit as described in [Permanent Limit](#permanent-limit). Then just be careful not to have the PP consume too much power when it’s low and connected to a power supply and the KB still has charging priority, as to not have the PP go down to 0% before it has a chance to finally receive power.

### Automatic suspension inhibited / setting `online` breaks reading/writing values

When the keyboard is attached to the phone and power output from the keyboard battery is not deactivated via the physical button or software, the phone will always rightfully think that it is connected to a power supply since that is what the phone battery reports. This includes ppkbbat-d or kbpwrd being active. Some ecosystems/distros that inhibit automatic suspension when the phone battery is being charged won’t let the phone automatically suspend in this case. Sxmo does not have this issue. My attempts to deactivate `Keyboard battery` -> `phone` charging automatically before suspend have been unsuccessful. Setting the `online` value of the keyboard battery can make it shut off entirely like the physical button after a bit, making it unable to reconnect via software. It also has a chance to break reading the keyboard battery values via sysfs. In that case everything else will still work, but you can’t read or modify the values anymore. Using `ppkb-i2c-charger-ctl power-off` to shut it off works perfectly fine in general, but I couldn’t get automation for this case to work. If you have this issue, either manually suspend the phone (e.g. `systemctl suspend`, I used that on Manjaro Plasma Mobile with a [keyboard shortcut](#ppkbbat-notif))or manually turn the keyboard battery off and on before and after suspend via the physical button or via [ppkbbat-toggle](#ppkbbat-toggle), maybe even using it similarly to a power bank instead.

### Wrong keyboard current while charging

Occasionally while charging, the keyboard reports some very wrong negative current instead of the positive current it should report. It’s not even just a wrong minus in front of the number, it’s completely wrong (ignore the obviously wrong KB capacity, that is common):

```
| 23:04:03 | PP Battery  | KB Battery  |
|----------+-------------+-------------|
| Current  |  -0.248000A |  -1.803380A |
| C. Limit |   1.200000A |   2.300000A |
| KB>PP L. |   0.500000A |             |
| Voltage  |   4.176000V |   4.222310V |
| OCV      |   4.217400V |   4.027070V |
| Capacity |        100% |        109% |
| Status   | Discharging |    Charging |
| Online   |           1 |           1 |
```

A power supply is connected and the keyboard limit is set to 2.3A, so it should receive +2.3A. Yet the sysfs value reports -1.8A. But when I also used `ppkb-i2c-charger-ctl` to check the current via i2c, it reported nearly the same number as the current limit, so everything is fine, only the sysfs value is bugged. As a result of this I added an option to ppkbbat-info that also displays the i2c values. This bug started happening as soon as I switched to Arch Sxmo with the new kernel driver in early May and it still happens now, mid June, albeit seemingly rarely. Now that I’ve resumed my battery testing I believe this only happened once and I don’t remember how I solved it. I believe it just fixed itself on its own after a while or something.

## Further Reading

- https://phal.io/tech/pinephone-keyboard
- https://xnux.eu/pinephone-keyboard/faq.html
- https://xnux.eu/pinephone-keyboard/
- https://xnux.eu/log/#toc-2021-05-26-pinephone-keyboard
- https://xnux.eu/log/#toc-more-chargers-more-issues
- https://xnux.eu/log/#065
- https://wiki.pine64.org/wiki/PinePhone_(Pro)_Keyboard
- https://www.pine64.org/2022/05/31/may-update-worth-the-wait/

[^1]: https://files.pine64.org/doc/PinePhone/USER%20MANUAL-KEYBOARD-V2-EN-DE-FR-ES.pdf
[^2]: https://wiki.pine64.org/wiki/PinePhone_(Pro)_Keyboard#Safety
[^3]: https://xnux.eu/pinephone-keyboard/faq.html#safety
[^4]: https://www.pine64.org/2022/05/31/may-update-worth-the-wait/ (Unfortunately I can’t link to the exact paragraph, scroll down to “PinePhone (Pro) Keyboard [by dsimic]”)
[^5]: https://xnux.eu/log/#072
[^6]: https://xnux.eu/pinephone-keyboard/faq.html#chargers
[^7]: I could have sworn this was in [Megi’s FAQ](https://xnux.eu/pinephone-keyboard/faq.html), saying 4.2V is full and 3V is empty, but I was unable to find it again.
